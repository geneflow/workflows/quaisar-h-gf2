============================
QuAISAR-H GeneFlow2 Workflow
============================

Version: v1.0.8-01

Quality, Assembly, Identification, Sequence type, Annotation, Resistance mechanisms for Hospital acquired infections (QuAISAR-H) is a mash-up of many publicly available tools with a splash of custom scripts with the purpose of producing a multi-layered quality checked report that identifies the taxonomy of and the Anti-microbial Resistence (AMR) elements from a paired end sequenced bacterial isolate. This version uses containers to ease the necessity of having many preinstalled tools.
